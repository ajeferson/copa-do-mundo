from os import system
from copaTeste.menu import *
from copaUtil.funcao import *
from copaUtil.copaArquivos import *
from copaTeste.test import *
from copaTeste.arquivo import *

def principal():
    """
    Desc. Exibe um menu interativo para a geração de valores aleatórios.
    """

    grupos = loadGrupos()
    eliminatorias = loadEliminatorias()
    todosJogos = [grupos, eliminatorias]
    rodada_corrente = qual_rod(todosJogos)
    while True:
        opt = menuInicialTeste(rodada_corrente[0])
        if opt=="1":
            geraJogosFaseGrupos()
            geraJogosEliminatorias()
            pontuacao(grupos)
            geraInfoSelecoes()
            grupos = loadGrupos()
            eliminatorias = loadEliminatorias()
            todosJogos = [grupos, eliminatorias]
            rodada_corrente = qual_rod(todosJogos)
            printColor.imprimir("\n  Arquivos resetados com sucesso.", "amarelo")
            printColor.imprimir("\n  Pressione <ENTER> para continuar.", "branco", False, False)
            fixInput(input())
        elif opt=="2":
            while True:
                try:
                    esc = int(menuGerarResultadosAleatorios(rodada_corrente[1]))
                    if esc == 0:
                        break
                    elif esc<=8:
                        if esc<=rodada_corrente[1]:
                            printColor.imprimir("\n  Opção não disponível.", "vermelho", False, False)
                            fixInput(input())
                        else:
                            geraResultados(esc, rodada_corrente[1])
                            grupos = loadGrupos()
                            eliminatorias = loadEliminatorias()
                            todosJogos = [grupos, eliminatorias]
                            rodada_corrente = qual_rod(todosJogos)
                            printColor.imprimir("\n  Rodada(s) avançada(s) com sucesso.", "amarelo")
                            printColor.imprimir("\n  Pressione <ENTER> para voltar", "branco", False, False)
                            fixInput(input())
                except ValueError:
                    pass
        
        elif opt=="0":
            printColor.imprimir("\n  Aplicação finalizada.", "amarelo")
            printColor.imprimir("\n  Pressione ENTER para sair.", "branco", False, False)
            fixInput(input())
            break

principal()
