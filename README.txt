************************************************************************
************************** Copa do Mundo 2014 **************************
************************************************************************

�Copa do Mundo 2014. All Rights Reserved.
Vers�o 1.0.1
Desenvolvido por: Alan Paiva e Vitor Muniz

Bugs e sugest�es, enviar e-mail para: 
- alan.jeferson11@gmail.com ou
- vitorfmuniz@gmail.com


**** Descri��o:
	Aplicativo feito para acompanhamento dos jogos da copa do mundo
FIFA 2014.

**** Features:
- Inser��o de resultados dos jogos;
- Acompanhamento dos jogos por rodadas;
- Apagar resultados;
- Tabela com a pontua��o;
- Sobre as sele��es;
- Busca por jogos. As op��es por data, sele��o, fase e grupos est�o dispon�veis;
- M�dulo de testes com gera��o de resultados aleat�rios.

**** Instru��es:
1. Para utilizar o aplicativo, � necess�rio que os arquivos sejam gerados primeiro,
para tal, execute o arquivo Instalador.py e siga o menu intuitivo para completar
a instala��o.
2. Ap�s instalado, o m�dulo principal e o m�dulo de testes estar�o dispon�veis.
No m�dulo principal, encontram-se as funcionalidades fundamentais do aplicativo, ou seja,
a inser��o de resultados, acompanhamento da tabela, busca por jogos, etc.
No m�dulo de testes est�o as op��es de desenvolvimento, como a gera��o de resultados e
pontua��o aleat�rios.

**** Nas pr�ximas vers�es:
- Mostrar as estat�sticas dos jogos (cart�es amarelos, faltas, etc.);
- Mostrar artilheiros da copa;
- "Aposta" de resultados;