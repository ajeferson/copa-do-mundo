from copaUtil.colorirTerminal import *
from copaUtil.funcao import fixInput
from os import system

#Global - Utilizada para fazer todos os prints coloridos
printColor = ColorirTerminal()

def cabecalhoTeste():
    """
    Desc.: Cabeçalho do menu interativo de testes
    """
    printColor.imprimir("\n  {}".format(62*"-"), "branco")
    printColor.imprimir("  {} Copa do Mundo 2014 {}".format(21*"-", 21*"-"), "branco")
    printColor.imprimir("  {} Módulo de testes {}".format(22*"-", 22*"-"), "branco")
    printColor.imprimir("  {}".format(62*"-"), "branco")
    

def menuInicialTeste(rodada):
    """
    Desc.: Exibe o menu inicial do módulo de testes
    """
    system("cls")
    cabecalhoTeste()
    printColor.imprimir("\n  Rodad atual: ", "branco", False, False)
    printColor.imprimir(rodada+"\n", "amarelo")
    printColor.imprimir("  1. Resetar arquivos", "branco")
    printColor.imprimir("  2. Avançar rodadas", "branco")
    printColor.imprimir("  3. Ajuda", "branco")
    printColor.imprimir("  4. Sobre", "branco")
    printColor.imprimir("  0. Sair", "branco")
    printColor.imprimir("\n  Escolha uma opção e pressione <ENTER>:", 'branco', False, False)
    return fixInput(input())

def menuGerarResultadosAleatorios(rodada):
    """
    Desc.: Exibe um menu com as opções de geração de resultados aleatórios.
    """
    system("cls")
    cabecalhoTeste()
    printColor.imprimir("\n  1. Fase de grupos/2ª rodada", "branco" if rodada<1 else "preto")
    printColor.imprimir("  2. Fase de grupos/3ª rodada", "branco" if rodada<2 else "preto")
    printColor.imprimir("  3. Eliminatórias/Oitavas de final", "branco" if rodada<3 else "preto")
    printColor.imprimir("  4. Eliminatórias/Quartas de final", "branco" if rodada<4 else "preto")
    printColor.imprimir("  5. Eliminatórias/Semi-final", "branco" if rodada<5 else "preto")
    printColor.imprimir("  6. Eliminatórias/Terceiro lugar", "branco" if rodada<6 else "preto")
    printColor.imprimir("  7. Eliminatórias/Final", "branco" if rodada<7 else "preto")
    printColor.imprimir("  8. Copa encerrada", "branco" if rodada<8 else "preto")
    printColor.imprimir("  0. Voltar", "branco")
    printColor.imprimir("\n  Escolha uma opção e pressione <ENTER>:", 'branco', False, False)
    return fixInput(input())

def menuPontuacaoAleatoria():
    """
    Desc.: Exibe o menu para escolha do número de rodadas cujo valor
    da pontuação aleatória deve ser gerado.
    """
    system("cls")
    cabecalhoTeste()
    printColor.imprimir("\n  Quantidade de rodadas:", 'branco', False, False)
    return fixInput(input())
    
