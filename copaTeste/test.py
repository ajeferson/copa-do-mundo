from pickle import dump, load
from copaUtil.copaArquivos import *
from copaUtil.funcao import *
from random import randint

def geraResultados(rodada, rodada_corrente):
    """
    Desc.: Gera resultados aleatórios entre 0 e 9 até a rodada passada como
    parâmetro. OBS: Falta implementar o gerador para as eliminatórias
    """

    #Limpando os arquivos
    
    #Carregando dados
    grupos = loadGrupos()
    eliminatorias = loadEliminatorias()
    todosJogos = [grupos, eliminatorias]
    tabela = loadSelecoesTabela()

    if(rodada>3):
        rodadasFaseGrupos = 3
        rodadasEliminatorias = rodada-3
    else:
        rodadasFaseGrupos = rodada
        rodadasEliminatorias = 0

    if rodada_corrente<3:
        for i in range(rodada_corrente, rodadasFaseGrupos):
            for k in range(len(grupos)):
                grupos[k][i][0][2]=randint(0,9)
                grupos[k][i][0][3]=randint(0,9)
                grupoAtualizado = atualizaTabela(grupos[k][i][0], tabela[k])
                tabela[k] = grupoAtualizado
                
                grupos[k][i][1][2]=randint(0,9)
                grupos[k][i][1][3]=randint(0,9)
                grupoAtualizado = atualizaTabela(grupos[k][i][1], tabela[k])
                tabela[k] = grupoAtualizado

            if i==2:
                tabela = ordenaTabela(tabela, grupos, random=False)
                todosJogos = atualizaOitavas(todosJogos, tabela, grupos)

    rodada_corrente = qual_rod(todosJogos)[1]

    if rodada_corrente>=3:
        rodada_corrente -= 3
        rodada -= 3
        for i in range(rodada_corrente, rodada):
            for k in range(len(todosJogos[1][i])):
                todosJogos[1][i][k][2]=randint(0,9)
                todosJogos[1][i][k][3]=randint(0,9)
            if i==0:
                todosJogos = atualizaQuartas(todosJogos)
            elif i==1:
                todosJogos = atualizaSemiFinal(todosJogos)
            elif i==2:
                todosJogos = atualizaTerceiroLugar(todosJogos)
                todosJogos = atualizaFinal(todosJogos)
            elif i==3 or i==4:
                todosJogos = atualizaPodium(todosJogos)

    #Salvando grupos
    saveGrupos(grupos)

    #Salvando eliminatórias
    saveEliminatorias(eliminatorias)

    #Salvando tabela
    tabela =  ordenaTabela(tabela, grupos)
    saveSelecoesTabela(tabela)


def geraPontuacao(rodadas):
    """
    Desc.: Gera pontuação, gols pró, gols contra, etc para os testes de tabela.
    """
    tabela = loadSelecoesTabela()

    pontuacao = [0, 1, 3]

    for i in range(rodadas):
        for grupo in tabela:
            for selecao in grupo:
                pont = pontuacao[randint(0,2)]
                selecao[1]=pont
                selecao[2]+=1
                if pont==3:
                    selecao[3]+=1
                elif pont==1:
                    selecao[4]+=1
                else:
                    selecao[5]+=1
                selecao[6]=randint(0,9)
                selecao[7]=randint(0,9)
    saveSelecoesTabela(ordenaTabela(tabela, grupos))
    
