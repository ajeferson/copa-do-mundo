#encoding:utf-8
import pickle
from copaUtil.funcao import ordenaTabela
from copaUtil.copaArquivos import saveGrupos, saveEliminatorias, saveSelecoesTabela, saveSelecoesInfo

def geraJogosFaseGrupos():
    """
    Desc.: Gera uma lista contendo todos os jogos da fase de grupos
    """
    #Jogos do grupo A 
    jogo3 = ["Brasil" , "México",-1,-1,-1,-1,[],[],[],[],-1,-1,[17,6],"Fortaleza"]
    jogo5 = ["Brasil" , "Camarões",-1,-1,-1,-1,[],[],[],[],-1,-1,[23,6],"Brasília"]
    jogo1 = ["Brasil" , "Croácia",-1,-1,-1,-1,[],[],[],[],-1,-1,[12,6],"São Paulo"]
    jogo2 = ["México" , "Camarões",-1,-1,-1,-1,[],[],[],[],-1,-1,[13,6],"Natal"]
    jogo6 = ["México" , "Croácia",-1,-1,-1,-1,[],[],[],[],-1,-1,[23,6],"Recife"]
    jogo4 = ["Camarões" , "Croácia",-1,-1,-1,-1,[],[],[],[],-1,-1,[18,6],"Manaus"]
    grupoA = [[jogo1 ,jogo2 ],[jogo3 ,jogo4 ],[jogo5 ,jogo6]] 

    #Jogos do grupo B
    jogo1 = ["Espanha" , "Holanda",-1,-1,-1,-1,[],[],[],[],-1,-1,[13,6],"Salvador"]
    jogo4 = ["Espanha" , "Chile",-1,-1,-1,-1,[],[],[],[],-1,-1,[18,6],"Rio de Janeiro"]
    jogo5 = ["Espanha" , "Austrália",-1,-1,-1,-1,[],[],[],[],-1,-1,[23,6],"Curitiba"]
    jogo2 = ["Chile"   , "Austrália",-1,-1,-1,-1,[],[],[],[],-1,-1,[13,6],"Cuiabá"]
    jogo6 = ["Chile"   , "Holanda",-1,-1,-1,-1,[],[],[],[],-1,-1,[23,6],"São Paulo"]
    jogo3 = ["Holanda" ,"Austrália" ,-1,-1,-1,-1,[],[],[],[],-1,-1,[18,6],"Porto Alegre"]
    grupoB = [[jogo1 ,jogo2 ],[jogo3 ,jogo4 ],[jogo5 ,jogo6]] 
     
    #Jogos do grupo C
    jogo5 = ["Colômbia" , "Japão",-1,-1,-1,-1,[],[],[],[],-1,-1,[24,6],"Cuiabá"]
    jogo3 = ["Colômbia" , "Costa do Marfim",-1,-1,-1,-1,[],[],[],[],-1,-1,[19,6],"Brasília"]
    jogo1 = ["Colômbia" , "Grécia",-1,-1,-1,-1,[],[],[],[],-1,-1,[14,6],"Belo Horizonte"]
    jogo2 = ["Costa do Marfim" , "Japão",-1,-1,-1,-1,[],[],[],[],-1,-1,[14,6],"Recife"]
    jogo6 = ["Costa do Marfim" , "Grécia",-1,-1,-1,-1,[],[],[],[],-1,-1,[24,6],"Fortaleza"]
    jogo4 = [ "Japão", "Grécia",-1,-1,-1,-1,[],[],[],[],-1,-1,[19,6],"Natal"]
    grupoC = [[jogo1 ,jogo2] ,[jogo3 ,jogo4] ,[jogo5 ,jogo6]] 

    #Jogos do grupo D
    jogo5 = ["Uruguai" , "Itália",-1,-1,-1,-1,[],[],[],[],-1,-1,[24,6],"Natal"]
    jogo1 = ["Uruguai" , "Costa Rica",-1,-1,-1,-1,[],[],[],[],-1,-1,[14,6],"Fortaleza"]
    jogo3 = ["Uruguai" , "Inglaterra",-1,-1,-1,-1,[],[],[],[],-1,-1,[19,6],"São Paulo"]
    jogo4 = ["Costa Rica" , "Itália",-1,-1,-1,-1,[],[],[],[],-1,-1,[20,6],"Recife"]
    jogo6 = ["Costa Rica" , "Inglaterra",-1,-1,-1,-1,[],[],[],[],-1,-1,[24,6],"Belo Horizonte"]
    jogo2 = ["Itália" , "Inglaterra",-1,-1,-1,-1,[],[],[],[],-1,-1,[14,6],"Manaus"]
    grupoD = [[jogo1 ,jogo2] ,[jogo3 ,jogo4] ,[jogo5 ,jogo6]] 

    #Jogos do grupo E
    jogo1 = ["Suíça" ,"Equador" ,-1,-1,-1,-1,[],[],[],[],-1,-1,[15,6],"Brasília"]
    jogo3 = ["Suíça" ,"França" ,-1,-1,-1,-1,[],[],[],[],-1,-1,[20,6],"Salvador"]
    jogo5 = ["Suíça" ,"Honduras" ,-1,-1,-1,-1,[],[],[],[],-1,-1,[25,6],"Manaus"]
    jogo6 = ["França" ,"Equador" ,-1,-1,-1,-1,[],[],[],[],-1,-1,[25,6],"Rio de Janeiro"]
    jogo2 = ["França" ,"Honduras" ,-1,-1,-1,-1,[],[],[],[],-1,-1,[15,6],"Porto Alegre"]
    jogo4 = ["Honduras" ,"Equador",-1,-1,-1,-1,[],[],[],[],-1,-1,[20,6],"Curitiba"]
    grupoE = [[jogo1 ,jogo2 ],[jogo3 ,jogo4 ],[jogo5 ,jogo6]] 

    #Jogos do grupo F
    jogo1 = ["Argentina" , "Bósnia",-1,-1,-1,-1,[],[],[],[],-1,-1,[15,6],"Rio de Janeiro"]
    jogo2 = ["Irã" , "Nigéria",-1,-1,-1,-1,[],[],[],[],-1,-1,[16,6],"Curitiba"]
    jogo3 = ["Argentina" ,  "Irã",-1,-1,-1,-1,[],[],[],[],-1,-1,[21,6],"Belo Horizonte"]
    jogo4 = ["Nigéria" , "Bósnia",-1,-1,-1,-1,[],[],[],[],-1,-1,[21,6],"Cuiabá"]
    jogo5 = ["Nigéria" ,"Argentina" ,-1,-1,-1,-1,[],[],[],[],-1,-1,[25,6],"Porto Alegre"]
    jogo6 = ["Bósnia" , "Irã",-1,-1,-1,-1,[],[],[],[],-1,-1,[25,6],"Salvador"]
    grupoF = [[jogo1 ,jogo2 ],[jogo3 ,jogo4 ],[jogo5 ,jogo6]] 

    #Jogos do grupo G
    jogo3 = ["Alemanha" , "Gana",-1,-1,-1,-1,[],[],[],[],-1,-1,[21,6],"Fortaleza"]
    jogo1 = ["Alemanha" , "Portugal",-1,-1,-1,-1,[],[],[],[],-1,-1,[16,6],"Salvador"]
    jogo5 = ["Alemanha" ,  "EUA",-1,-1,-1,-1,[],[],[],[],-1,-1,[26,6],"Recife"]
    jogo4 = ["Portugal" , "EUA",-1,-1,-1,-1,[],[],[],[],-1,-1,[22,6],"Manaus"]
    jogo6 = ["Portugal" ,"Gana" ,-1,-1,-1,-1,[],[],[],[],-1,-1,[26,6],"Brasília"]
    jogo2 = ["Gana" , "EUA",-1,-1,-1,-1,[],[],[],[],-1,-1,[16,6],"Natal"]
    grupoG = [[jogo1 ,jogo2 ],[jogo3 ,jogo4 ],[jogo5 ,jogo6]] 

    #Jogos do grupo H
    jogo3 = ["Bélgica" , "Rússia",-1,-1,-1,-1,[],[],[],[],-1,-1,[22,6],"Rio de Janeiro"]
    jogo5 = ["Bélgica" , "Coréia do Sul",-1,-1,-1,-1,[],[],[],[],-1,-1,[26,6],"São Paulo"]
    jogo1 = ["Bélgica" , "Argélia",-1,-1,-1,-1,[],[],[],[],-1,-1,[17,6],"Belo Horizonte"]
    jogo4 = ["Argélia" ,"Coréia do Sul",-1,-1,-1,-1,[],[],[],[],-1,-1,[22,6],"Porto Alegre"]
    jogo6 = ["Argélia" ,"Rússia" ,-1,-1,-1,-1,[],[],[],[],-1,-1,[26,6],"Curitiba"]
    jogo2 = ["Rússia" , "Coréia do Sul",-1,-1,-1,-1,[],[],[],[],-1,-1,[17,6],"Cuiabá"]
    grupoH = [[jogo1 ,jogo2 ],[jogo3 ,jogo4 ],[jogo5 ,jogo6]]

    grupos = [grupoA, grupoB, grupoC, grupoD, grupoE, grupoF, grupoG, grupoH]
    saveGrupos(grupos)

def geraJogosEliminatorias():
    """
    Desc.: Retorna uma lista contendo os jogos da fase eliminatória
    """
    
    #Jogos das oitavas de final
    jogos81 = ["","",-1,-1,-1,-1,[],[],[],[],-1,-1,[28,6],"Belo Horizonte"]
    jogos82 = ["","",-1,-1,-1,-1,[],[],[],[],-1,-1,[28,6],"Rio de janeiro"]
    jogos83 = ["","",-1,-1,-1,-1,[],[],[],[],-1,-1,[29,6],"Fortaleza"]
    jogos84 = ["","",-1,-1,-1,-1,[],[],[],[],-1,-1,[29,6],"Recife"]
    jogos85 = ["","",-1,-1,-1,-1,[],[],[],[],-1,-1,[30,6],"Brasília"]
    jogos86 = ["","",-1,-1,-1,-1,[],[],[],[],-1,-1,[30,6],"Porto Alegre"]
    jogos87 = ["","",-1,-1,-1,-1,[],[],[],[],-1,-1,[1,7],"São Paulo"]
    jogos88 = ["","",-1,-1,-1,-1,[],[],[],[],-1,-1,[1,7],"Salvador"]
    fase8 = [jogos81,jogos82,jogos83,jogos84,jogos85,jogos86,jogos87,jogos88]

    #Jogos das quartas de final
    jogos41 = ["","",-1,-1,-1,-1,[],[],[],[],-1,-1,[4,7],"Fortaleza"]
    jogos42 = ["","",-1,-1,-1,-1,[],[],[],[],-1,-1,[4,7],"Rio de Janeiro"]
    jogos43 = ["","",-1,-1,-1,-1,[],[],[],[],-1,-1,[5,7],"Salvador"]
    jogos44 = ["","",-1,-1,-1,-1,[],[],[],[],-1,-1,[5,7],"Brasília"]
    fase4 = [jogos41,jogos42,jogos43,jogos44]

    #Jogos das semi-finais
    jogoSF1 = ["","",-1,-1,-1,-1,[],[],[],[],-1,-1,[8,7],"Belo Horizonte"]
    jogoSF2 = ["","",-1,-1,-1,-1,[],[],[],[],-1,-1,[9,7],"São Paulo"]
    faseSF = [jogoSF1,jogoSF2]

    #Terceiro lugar
    terc_lugar = ["","",-1,-1,-1,-1,[],[],[],[],-1,-1,[12,7],"Brasília"]

    #Final
    final = ["","",-1,-1,-1,-1,[],[],[],[],-1,-1,[13,7],"Rio de Janeiro"]

    eliminatorias = [fase8,fase4,faseSF,[terc_lugar],[final],["", "", ""]]
    saveEliminatorias(eliminatorias)

def pontuacao(grupos):
    """
    Desc.: Gera uma lista contendo todas as informações da tabela da fase de grupos
    """
    # Pontuação do Grupo A
    br = ["Brasil",0,0,0,0,0,0,0]
    cro=["Croácia",0,0,0,0,0,0,0]
    mex = ["México",0,0,0,0,0,0,0]
    cam = ["Camarões",0,0,0,0,0,0,0]
    
    # Pontuação do Grupo B
    esp = ["Espanha",0,0,0,0,0,0,0]
    hol = ["Holanda",0,0,0,0,0,0,0]
    chi = ["Chile",0,0,0,0,0,0,0]
    aus = ["Austrália",0,0,0,0,0,0,0]
   
    #Pontuação do Grupo C
    col = ["Colômbia",0,0,0,0,0,0,0]
    gre = ["Grécia",0,0,0,0,0,0,0]
    costmarf = ["Costa do Marfim",0,0,0,0,0,0,0]
    jap = ["Japão",0,0,0,0,0,0,0]
    
    #Pontuação do Grupo D
    uru = ["Uruguai",0,0,0,0,0,0,0]
    costrica=["Costa Rica",0,0,0,0,0,0,0]
    ing = ["Inglaterra",0,0,0,0,0,0,0]
    ita = ["Itália",0,0,0,0,0,0,0]
    
    # Pontuação do Grupo E
    sui = ["Suíça",0,0,0,0,0,0,0]
    equ = ["Equador",0,0,0,0,0,0,0]
    fr  = ["França",0,0,0,0,0,0,0]
    hon = ["Honduras",0,0,0,0,0,0,0]
    
    # Pontuação do Grupo F
    arg = ["Argentina",0,0,0,0,0,0,0]
    bos = ["Bósnia",0,0,0,0,0,0,0]
    ira = ["Irã",0,0,0,0,0,0,0]
    nig = ["Nigéria",0,0,0,0,0,0,0]
    
    # Pontuação do Grupo G
    ale = ["Alemanha",0,0,0,0,0,0,0]
    port = ["Portugal",0,0,0,0,0,0,0]
    gan = ["Gana",0,0,0,0,0,0,0]
    eua = ["EUA",0,0,0,0,0,0,0]
    
    # Pontuação do Grupo H # Pontuação do Grupo A
    bel = ["Bélgica",0,0,0,0,0,0,0]
    argli =["Argélia",0,0,0,0,0,0,0]
    rus = ["Rússia",0,0,0,0,0,0,0]
    core = ["Coréia do Sul",0,0,0,0,0,0,0]
    
    #Listas das pontuações
    pontGA = [br,mex,cro,cam]
    pontGB = [esp,hol,chi,aus]
    pontGC = [col,gre,costmarf,jap]
    pontGD =[uru,costrica,ing,ita]
    pontGE = [sui,equ,fr,hon]
    pontGF = [arg, bos,ira,nig]
    pontGG = [ale,port,gan,eua]
    pontGH = [bel,argli,rus,core]
    #lista da tabela
    tabelapont = [pontGA,pontGB,pontGC,pontGD,pontGE,pontGF,pontGG,pontGH]
    
    saveSelecoesTabela(ordenaTabela(tabelapont, grupos, random=False))
    

def geraInfoSelecoes():
    """
    Desc.: Gera uma lista contendo as informações das seleções da copa do mundo
    """
    ##As informações das listas a seguir obedecem a seguinte ordem:
    
    
    # Informacoes do Grupo A
    br = ["Brasil","Luiz Felipe Scolari","Pelé","19","5","9","Brasil é o maior país da América do Sul e da região da América Latina,sendo o quinto maior do mundo em área territorial equivalente a 47% do território sul-americano e possui uma população com mais de 201 milhões de habitantes."]
    cro=["Croácia","Niko Kovac","Davor Suker","3","0","16","A Croácia é um país europeu que limita ao norte com a Eslovénia e Hungria, a nordeste com a Sérvia, a leste com a Bósnia e Herzegovina e ao sul com Montenegro. É banhado a oeste pelo mar Adriático e possui uma fronteira marítima com a Itália, no golfo de Trieste."]
    mex = ["México","Miguel Herrera","Jared Borgetti","14","0","20","México é uma república constitucional federal localizada na América do Norte. O país é limitado a norte pelos Estados Unidos; ao sul e oeste pelo Oceano Pacífico; a sudeste pela Guatemala, Belize e Mar do Caribe; a leste pelo Golfo do México. Com um território que abrange quase 2 milhões de quilômetros quadrados, o México é o quinto maior país das Américas por área total e o 14º maior país independente do mundo. Com uma população estimada em 118 milhões de habitantes."]
    cam = ["Camarões","Volker Finke","Samuel Eto'o","5","0","50","Camarões é um país africano, limitado a oeste e a norte pela Nigéria, a leste pelo Chade e pela República Centro-Africana, a sul pelo Congo, pelo Gabão e por Rio Muni (Guiné Equatorial) e a oeste pelo Golfo da Guiné, através do qual faz fronteira com a Guiné Equatorial, via a ilha de Bioko.O país possui uma população estimada de 19 294 149 pessoas."]
    
    # Informacoes do Grupo B
    esp = ["Espanha","Vicente del Bosque","David Villa","12","1","1","Espanha é um país situado na Europa meridional, na Península Ibérica. Seu território principal é delimitado a sul e a leste pelo Mar Mediterrâneo, com exceção a uma pequena fronteira com o território britânico ultramarino de Gibraltar; ao norte pela França, Andorra e pelo Golfo da Biscaia e ao noroeste e oeste pelo Oceano Atlântico e por Portugal.A Espanha é o segundo maior país da Europa Ocidental. Possui uma população com mais de 47 milhões de pessoas"]
    hol = ["Holanda","Louis van Gaal","Robin van Persie","10","0","11","Países Baixos é uma nação constituinte do Reino dos Países Baixos localizada na Europa ocidental. O país faz fronteira com a Bélgica ao sul e com a Alemanha a leste. Os Países Baixos são frequentemente chamados de Holanda, o que é tecnicamente impreciso, já que as Holandas do Norte e do Sul são duas de suas doze províncias. O gentílico holandês é o normalmente utilizado para se referir ao povo, à língua e a qualquer coisa que pertença aos Países Baixos, embora mantenha a ambiguidade. Neerlandês é o gentílico não ambíguo, alternativo. Possui uma população estimada em 16.742.993 de pessoas e 42.437 quilômetros quadrados."]
    chi = ["Chile","Jorge Sampaoli","Marcelo Salas","8","0","15","Chile,é um país da América do Sul, que ocupa uma longa e estreita faixa costeira encravada entre a cordilheira dos Andes e o oceano Pacífico. Faz fronteira ao norte com o Peru, a nordeste com a Bolívia, a leste com a Argentina e a Passagem de Drake, a ponta mais meridional do país. É um dos dois únicos países da América do Sul que não tem uma fronteira comum com o Brasil, além do Equador. O Pacífico forma toda a fronteira oeste do país, com um litoral que se estende por 6 435 quilômetros. O Chile possui um território incomum, com 4 300 quilômetros de comprimento e, em média, 175 quilômetros de largura e uma população com mais de 15,1 milhões de pessoas."]
    aus = ["Austrália","Ange Postecoglou","John Kosmina","3","0","63","Austrália é um país do hemisfério sul, localizado na Oceania, que compreende a menor área continental do mundo com cerca de 7.7 milhões de quilômetros quadrados, a ilha da Tasmânia e várias ilhas adjacentes nos oceanos Índico e Pacífico. O continente-ilha, como a Austrália por vezes é chamada, é banhado pelo oceano Índico, a sul e a oeste, pelo mar de Timor, mar de Arafura e Estreito de Torres, a norte, e pelo mar de Coral e mar da Tasmânia, a leste. Através destes mares, tem fronteira marítima com a Indonésia, Timor-Leste e Papua-Nova Guiné, a norte, e com o território francês da Nova Caledónia, a leste, e a Nova Zelândia a sudeste. A população do país é de 23,4 milhões de habitantes."]
   
    #Informacoes do Grupo C
    col = ["Colômbia","José Pekerman","Arnoldo Iguarán","4","0","5","Colômbia é um país da América do Sul.Possui uma extensão territorial com cerca de 1.2 milhões de quilômetros quadrados e faz fronteira a leste com a Venezuela e Brasil; ao sul com o Equador e Peru; para o norte com o Mar do Caribe, ao noroeste com o Panamá; e a oeste com o Oceano Pacífico. A Colômbia também tem fronteiras marítimas com a Venezuela,Jamaica, Haiti, República Dominicana, Honduras, Nicarágua e Costa Rica. Com uma população de mais de 45 milhões de pessoas, a Colômbia tem a 29ª maior população do mundo e a segunda maior da América do Sul, depois do Brasil."]
    gre = ["Grécia","Fernando Santos","Nikos Anastopoulos","2","0","13","Grécia é um país localizado no sul da Europa.  A população grega é de cerca de 11 milhões de pessoas e a extensão territorial do país é cerca de 132 mil quilômetros quadrados. O país está estrategicamente localizado no cruzamento entre a Europa, a Ásia, o Oriente Médio e a África. Tem fronteiras terrestres com a Albânia a noroeste, com a República da Macedônia e a Bulgária ao norte e com a Turquia no nordeste."]
    costmarf = ["Costa do Marfim","Sabri Lamouchi","Didier Drogba","2","0","24","A Costa do Marfim é um país africano, limitado a norte pelo Mali e pelo Burkina Faso, a leste pelo Gana, a sul pelo Oceano Atlântico e a oeste pela Libéria e pela Guiné. Possui extensão territorial com cerca de 322 mil quilômetros quadrados e uma população com aproximadamente 20 milhões habitantes"]
    jap = ["Japão","Alberto Zaccheroni","Kunishige Kamamoto","5","0","48","Japão é um país insular da Ásia Oriental. Localizado no Oceano Pacífico, a leste do Mar do Japão, da República Popular da China, da Coreia do Norte, da Coreia do Sul e da Rússia, se estendendo do Mar de Okhotsk, no norte, ao Mar da China Oriental e Taiwan, ao sul. Os caracteres que compõem seu nome significam Origem do Sol, razão pela qual o Japão é às vezes identificado como a Terra do Sol Nascente. O país possui uma extensão territorial com cerca de 377.815  quilômetros quadrados e uma população estimada de 127.400.000 de pessoas."]
    
    #Informacoes do Grupo D
    uru = ["Uruguai","Óscar Tabárez","Luis Suárez","11","2","6","Uruguai é um país localizado na parte sudeste da América do Sul. Possui de cerca de 3.5 milhões de Habitantes e extensão territorial com cerca de 176 mil quilômetros quadrados de área territorial . A única fronteira terrestre do Uruguai é com o estado brasileiro do Rio Grande do Sul, no norte. Para o oeste encontra-se o rio Uruguai e a sudoeste situa-se o estuário do rio da Prata. O país faz fronteira com a Argentina apenas em alguns bancos de qualquer um dos rios citados acima, enquanto que a sudeste fica o Oceano Atlântico. O Uruguai é o segundo menor país da América do Sul, sendo somente maior que o Suriname."]
    costrica=["Costa Rica","Jorge Luis Pinto","Rolando Fonseca","3","0","34","Costa Rica é um país da América Central limitado a norte pela Nicarágua, a leste pelo mar do Caribe, a sudeste pelo Panamá e a oeste pelo oceano Pacífico. É também costarriquenha a Ilha do Coco, no mesmo oceano. Possui uma extensão territorial com cerca de 51 mil quilômetros quadrados e uma população por volta de 4.5 milhões de  habitantes."]
    ing = ["Inglaterra","Roy Hodgson","Bobby Charlton","12","1","12","Inglaterra é uma das nações constituintes do Reino Unido. O país faz fronteira com a Escócia ao norte e com o País de Gales a oeste; o Mar da Irlanda está a noroeste, o Mar Celta está a sudoeste, enquanto o Mar do Norte está a leste e o Canal da Mancha, ao sul, a separa da Europa continental. A maior parte da Inglaterra compreende a parte central e sul da ilha da Grã-Bretanha, no Atlântico Norte.Possui uma população com cerca de 51.000.000 de pessoas e extensão territorial de  130.395  quilômetros quadrados."]
    ita = ["Itália","Cesare Prandelli","Luigi Riva","18","4","8","Itália é um país localizado no centro-sul da Europa (Europa meridional). Ao norte, faz fronteira com França, Suíça, Áustria e Eslovênia ao longo dos Alpes. Ao sul, que consiste na totalidade da península Itálica, Sicília, Sardenha, as duas maiores ilhas no Mar Mediterrâneo, e muitas outras ilhas menores ficam no entorno do território italiano. Os Estados independentes de San Marino e do Vaticano são enclaves no interior de Itália, enquanto Campione d'Italia é um enclave italiano na Suíça. O território do país abrange cerca de 301.338 quilômetros quadrados e Com uma população com cerca de 60.600.000 habitantes"]
    
    #Informacoes do Grupo E
    sui = ["Suíça","Ottmar Hitzfeld","Alexander Frei","8","0","7","Suíça é um país que está situado na Europa Central, onde faz fronteira com a Alemanha a Norte, com a França a Oeste, com Itália a Sul e com a Áustria e Liechtenstein a Leste.Possui uma extensão territorial com cerca de 41 285 quilômetros quadrados e possui uma  população com  aproximadamente 7.8 milhões habitantes."]
    equ = ["Equador","Reinaldo Rueda","Agustín Delgado","2","0","23","Equador é um país localizado na América do Sul limitado ao norte pela Colômbia, a leste e ao sul pelo Peru e a oeste pelo Oceano Pacífico. É um dos dois países da região que não tem fronteiras comuns com o Brasil, além do Chile. Além do território continental, o Equador possui também as ilhas Galápagos, a cerca de 1000 km do território continental, sendo o país mais próximo daquelas ilhas. Seu território tem aproximadamente 256 mil quilômetros quadrados  e possui uma população com cerca de 15.007.343 pessoas."]
    fr  = ["França","Didier Deschamps","Thierry Henry","13","1","17","França é um país localizado na Europa Ocidental, com várias ilhas e territórios ultramarinos noutros continentes. A França Metropolitana se estende do Mediterrâneo ao Canal da Mancha e Mar do Norte, e do Rio Reno ao Oceano Atlântico. A extensão territorial é cerca de 675 mil quilômetros quadrados e posui uma população com aproximadamente 65.4 milhões de pessoas."]
    hon = ["Honduras","Luis Fernando Suárez","Carlos Pavón","2","0","36","Honduras é um país da América Central, limitado a norte pelo Golfo das Honduras, a norte e a leste pelo Mar das Caraíbas (por onde possui fronteira marítima com o território colombiano de San Andrés e Providencia), a sul pela Nicarágua, pelo Golfo de Fonseca e por El Salvador e a oeste pela Guatemala.Possui um área com cerca de  112 mil quilômetros quadrados  e uma população com aproximadamente 7.5 milhões de habitantes."]
    
    #Informacoes do Grupo F
    arg = ["Argentina","Alejandro Sabella","Gabriel Batistuta","15","2","3","Argentina é um país localizado na América do Sul .Possui extensão territorial com cerca de 2.8 milhões de quilômetros quadrados e população de 40.276.376 habitantes."]
    bos = ["Bósnia","Safet Susic","Edin Dzeko","0","0","21","Bósnia e Herzegovina (ou de forma abreviada Bósnia) é um país localizado nos  Bálcãs, resultante da dissolução da Jugoslávia, limitada a norte e oeste pela Croácia, a leste e a sul pela Sérvia, e a sul pelo Montenegro, dispondo ainda de uma minúscula extensão de litoral, no Mar Adriático.possui uma  extensão territorial de 51.129  quilômetros quadrados e uma população com cerca de 3.8 milhões de habitantes."]
    ira = ["Irã","Carlos Queiroz","Ali Daei","3","0","42"," Irã  é um país localizado na Ásia Ocidental. Tem fronteiras a norte com Arménia, Azerbaijão e Turquemenistão e com o Cazaquistão e a Rússia através do Mar Cáspio; a leste com Afeganistão e Paquistão; ao sul com o Golfo Pérsico e o Golfo de Omã; a oeste com o Iraque; e a noroeste com a Turquia. Composto por uma área de 1 648 195 quilômetros quadrados  e uma população com mais de 77 milhões de habitantes."]
    nig = ["Nigéria","Stephen Keshi","Rashidi Yekini","4","0","47","Nigéria é um  país é localizado na África Ocidental e compartilha fronteiras terrestres com Benim a oeste, Chade e Camarões a leste e Níger a norte. Sua costa está no Golfo da Guiné, uma parte do Oceano Atlântico, ao sul. Possui uma população com cerca de 141 milhões de habitantes e uma extensão territorial com cerca de 924 mil quilômetros quadrados."]
    
    #Informacoes do Grupo G
    ##o caso da alemanha os dois jogadores tem a mesma quantidade de gols
    ale = ["Alemanha","Joachim Low","Gerd Muller,Miroslav Klose","17","3","2","Alemanha é um país localizado na Europa central. É limitada a norte pelo mar do Norte, Dinamarca e pelo mar Báltico, a leste pela Polônia e pela República Checa, a sul pela Áustria e pela Suíça e a oeste pela França, Luxemburgo, Bélgica e Países Baixos. Possui uma extensão territorial com cerca de Alemanha 357 mil quilômetros quadrados e uma população de 81.8 milhões de habitantes."]
    port = ["Portugal","Paulo Bento","Cristiano Ronaldo","5","0","4","Portugal é um país   localizado no Sudoeste da Europa, cujo território se situa na zona ocidental da Península Ibérica e em arquipélagos no Atlântico Norte ,sendo delimitado a norte e leste pela Espanha e sul e oeste pelo oceano Atlântico. Possui uma extensão territorial com cerca de 92 mil quilômetros quadrados  e uma população com aproximadamente 155 milhões de pessoas."]
    gan = ["Gana","James Kwesi Appiah","Abédi Pelé","2","0","35","Gana é um país da África ocidental, limitado a norte pelo Burkina Faso, a leste pelo Togo, a sul pelo Golfo da Guiné e a oeste pela Costa do Marfim.  Possui uma extensão territorial aproximadamente de 239 mil quilômetros quadrados e uma população com cerca de 24 milhões de habitantes."]
    eua = ["EUA","Jurgen Klinsmann","Landon Donovan","9","0","14","Estados Unidos da América. A maior parte do país situa-se na região central da América do Norte, formada por 48 estados e Washington, D.C., o distrito federal da capital. Localiza-se entre os oceanos Pacífico e Atlântico, fazendo fronteira com o Canadá ao norte e com o México ao sul. O estado do Alasca está no noroeste do continente, fazendo fronteira com o Canadá no leste e com a Rússia a oeste, através do estreito de Bering.Com 9,37 milhões de km² de área e cerca de 309 milhões de habitante."]
    
    #Informacoes do Grupo H
    ## o caso da belgica os dois jogadores tem a mesma quantdade de gols
    bel = ["Bélgica","Marc Wilmots","Paul Van Himst,Bernard Voorhoof","11","0","10","Bélgica  é um país situado na Europa ocidental. A Bélgica tem uma extensão territorial de aproximadamente  31 mil quilômetros quadrados e uma população de cerca de 10,7 milhões de habitantes."]
    argli =["Argélia","Vahid Halilhodzic","Abdelhafid Tasfaout","3","0","25","Argélia é um país da África do Norte que faz parte do Magreb. Possui uma extensão territorial de aproximadamente 2.4 milhões de quilômetros quadrados e uma população com cerca de 34 milhões de habitantes. Partilha suas fronteiras terrestres ao nordeste com a Tunísia, a leste com a Líbia, ao sul com o Níger e o Mali, a sudoeste com a Mauritânia e o território contestado do Saara Ocidental, e ao oeste com Marrocos."]
    rus = ["Rússia","Fabio Capello","Vladimir Beschastnykh","2","0","19","Rússia é um país localizado no norte da Eurásia. Faz fronteira com os seguintes países, de noroeste para sudeste: Noruega, Finlândia, Estônia, Letônia, Lituânia e Polônia (ambas através do exclave de Kaliningrado),Bielorrússia, Ucrânia, Geórgia, Azerbaijão, Cazaquistão, China, Mongólia e Coreia do Norte. Faz também fronteiras marítimas com oJapão, pelo Mar de Okhotsk, e com os Estados Unidos, pelo Estreito de Bering. Com 17.075.400 de quilômetros quadrados, a Rússia é o país com maior área do planeta, cobrindo mais de um nono da área terrestre. É também o nono país mais populoso, com 142 milhões de habitantes. "]
    core = ["Coréia do Sul","Choi Kang-Hee","Cha Bum-Kun","8","0","60","Coreia do Sul é um país da Ásia Oriental, localizado na parte sul da Península da Coreia. Sua única fronteira terrestre é com a Coreia do Norte, com a qual formou apenas um país até 1945. Faz fronteira a leste com o Mar do Japão, a sul com o Estreito da Coreia, que o separa do Japão, e a oeste com o Mar Amarelo.Possui uma extensão territorial com cerca de 100 mil quilômetros quadrados e uma população com quase 50 milhões de habitantes. "]
    
    #lista da tabela de informacoes
    tabelainfo = [ale, arg, argli, aus, bel, bos, br, cam, chi, col, core, costmarf, costrica, cro, equ, esp, eua, fr, gan, gre, hol, hon, ing, ira, ita, jap, mex, nig, port, rus, sui, uru]
    saveSelecoesInfo(tabelainfo)
