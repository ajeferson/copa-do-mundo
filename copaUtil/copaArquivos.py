import pickle

def loadGrupos():
    """
    Desc.: Carrega em uma única lista todos arquivos que contém
    um grupo de jogos da copa.
    """

    try:
        prefixo = "arquivos"
        f = open("{}/grupos.arq".format(prefixo),"rb")
    except IOError:
        prefixo = "../arquivos"
        
    #Grupo A
    f = open("{}/grupos.arq".format(prefixo),"rb")
    grupos = pickle.load(f)
    f.close()

    return grupos

def loadEliminatorias():
    """
    Desc.: Abre o arquivo dos jogos das eliminatorias e retorna
    uma lista contendo os mesmos
    """

    try:
        prefixo = "arquivos"
        f = open("{}/eliminatorias.arq".format(prefixo),"rb")
    except IOError:
        prefixo = "../arquivos"
    
    f = open("{}/eliminatorias.arq".format(prefixo),"rb")
    eliminatorias = pickle.load(f)
    f.close()
    return eliminatorias

def saveGrupos(grupos):
    """
    Desc.: Recebe a lista dos grupos como parametro e salva elas em arquivo
    """

    try:
        prefixo = "arquivos"
        f = open("{}/grupos.arq".format(prefixo),"wb")
    except IOError:
        prefixo = "../arquivos"
        
    #Grupo A
    f = open("{}/grupos.arq".format(prefixo),"wb")
    pickle.dump(grupos, f)
    f.close()


def saveEliminatorias(eliminatorias):
    """
    Desc.: Recebe uma lista contendo os jogos das eliminatórias
    e a salva em arquivo.
    """
    try:
        prefixo = "arquivos"
        f = open("{}/eliminatorias.arq".format(prefixo),"wb")
    except IOError:
        prefixo = "../arquivos"
        
    f = open("{}/eliminatorias.arq".format(prefixo),"wb")
    pickle.dump(eliminatorias,f)
    f.close()

def loadSelecoesTabela():
    """
    Desc.: Carrega e retorna uma lista com os dados da selecoes
    """
    try:
        prefixo = "arquivos"
        f = open("{}/tabela_pont.arq".format(prefixo),"rb")
    except:
        prefixo = "../arquivos"
    f = open("{}/tabela_pont.arq".format(prefixo),"rb")
    tabela = pickle.load(f)
    f.close()
    return tabela

def saveSelecoesTabela(tabela):
    """
    Desc.: Recebe uma lista que representa a tabela da copa
    e a salva em arquivo.
    """
    try:
        prefixo = "arquivos"
        f = open("{}/tabela_pont.arq".format(prefixo),"wb")
    except:
        prefixo = "../arquivos"
    f = open("{}/tabela_pont.arq".format(prefixo),"wb")
    pickle.dump(tabela,f)
    f.close()

def saveSelecoesInfo(selecoes):
    """
    Desc.: Salva um arquivo com as informações das seleções.
    """
    try:
        prefixo = "arquivos"
        f = open("{}/info_selecoes.arq".format(prefixo),"wb")
    except:
        prefixo = "../arquivos"
    f = open("{}/info_selecoes.arq".format(prefixo),"wb")
    pickle.dump(selecoes,f)
    f.close()

def loadSelecoesInfo():
    """
    Def.: Carrega os arquivos com as informações das seleções
    """
    try:
        prefixo = "arquivos"
        f = open("{}/info_selecoes.arq".format(prefixo),"rb")
    except:
        prefixo = "../arquivos"
    f = open("{}/info_selecoes.arq".format(prefixo),"rb")
    lista = pickle.load(f)
    f.close()
    return lista
