from copaUtil.colorirTerminal import *
from os import system
from copaUtil.funcao import *

#Global - Utilizada para fazer todos os prints coloridos
printColor = ColorirTerminal()

def cabecalho():
    """
    Desc.: Imprime na tela o cabecalho com o nome de programa
    """
    printColor.imprimir("\n  {}".format(66*"-"), "branco")
    printColor.imprimir("  {} Copa do Mundo 2014 {}".format(23*"-", 23*"-"), "branco")
    printColor.imprimir("  {}".format(66*"-"), "branco")

def menuInicial(todosJogos, rodada):
    """
    Apaga o que tiver na tela e mostra o menu inicial.
    Retorna o input do teclado da opção escolhida pelo usuário.
    """
    system("cls")
    cabecalho()
    printColor.imprimir("\n  Rodada atual: ", "branco", False, False)
    printColor.imprimir(rodada[0]+"\n", "amarelo")
    printColor.imprimir("  1. Buscar jogos", "branco")
    printColor.imprimir("  2. Editar jogos", "branco" if rodada[1]<8 else "preto")
    printColor.imprimir("  3. Tabela", "branco")
    printColor.imprimir("  4. Podium", "branco" if rodada[1]==8 else "preto")
    printColor.imprimir("  5. Informações sobre seleções", "branco")
    printColor.imprimir("  6. Sobre", "branco")
    printColor.imprimir("  0. Sair", "branco")
    printColor.imprimir("\n  Escolha uma opção e pressione <ENTER>:", 'branco', False, False)
    return fixInput(input())

def menuListarJogos(rodada):
    """
    Desc.: Mostra o menu dos jogos da rodada retorna o valor de uma tecla
    da escolha do usuário.
    """
    system("cls")
    cabecalho()
    printColor.imprimir("\n  Rodada atual: ", "branco", False, False)
    printColor.imprimir("{}\n".format(rodada), "amarelo")
    printColor.imprimir("  1. Data","branco",False,False)
    printColor.imprimir("\n  2. Seleções","branco",False,False)
    printColor.imprimir("\n  3. Grupos","branco",False,False)
    printColor.imprimir("\n  4. Fases","branco",False,False)
    printColor.imprimir("\n  0. Voltar ","branco",False,False)
    printColor.imprimir("\n\n  Escolha uma opção e pressione <ENTER>:", 'branco', False, False)
    return fixInput(input())

def printajogos(jogos):
    """
    Desc.:Recebe uma lista com os jogos e retorna os jogos 'printados'.
    """
    for j in jogos:
        try:
            printColor.imprimir("  {:>15} {} X {} {:<15}   {:0>2}/{:0>2}   {:>14}".format(j[0] if j[0]!="" else "Indefinido",j[2] if j[2] >=0 else " ",
                    j[3] if j[3] >=0 else " ", j[1] if j[1]!="" else "Indefinido",j[12][0], j[12][1], j[13]),"branco")
        except IndexError:
            for i in j:
                printColor.imprimir("  {:>15} {} X {} {:<15}   {:0>2}/{:0>2}   {:>14}".format(i[0] if i[0]!="" else "Indefinido",i[2] if i[2] >=0 else " ",
                    i[3] if i[3] >=0 else " ",i[1] if i[1]!="" else "Indefinido" ,i[12][0],i[12][1],i[13]),"branco")
            


def menuJogosRodada(todosJogos, rodada):
    """
    Desc.: Mostra o menu com os jogos da rodada, que podem ser editados pelo
    usuário. Retorna o index do jogo a ser editado.
    """
    counter = 1
    grupos = todosJogos[0]
    eliminatorias = todosJogos[1]
    first = 65
    invalidos = []
    system("cls")
    cabecalho()
    printColor.imprimir("\n  Rodada corrente: ", "branco", False, False)
    printColor.imprimir(rodada[0], "amarelo")

    if rodada[1]<=2:
        for grupo in grupos:
            printColor.imprimir("\n  {} Grupo {} {}\n".format(28*"-", chr(first), 29*"-"), "amarelo")
            for jogo in grupo[rodada[1]]:
                if(jogo[2]==-1):
                    cor = "branco"
                else:
                    cor = "preto"
                    invalidos.append(counter)
                printColor.imprimir("  {:>2}. {:>15} {} X {} {:<15}   {:0>2}/{:0>2}   {:>14}".format(counter, jogo[0],
                                jogo[2] if jogo[2] >=0 else " ", jogo[3] if jogo[3] >=0 else " ", jogo[1],
                                jogo[12][0], jogo[12][1], jogo[13]), cor)
                counter+=1
            first+=1
    else:
        rod = rodada[1]-2
        printColor.imprimir("\n  ----- {:-<60}\n".format("{} ".format(rodada[0])), "azul")
        for jogo in eliminatorias[rod-1]:
            if(jogo[2]==-1):
                cor = "branco"
            else:
                cor = "preto"
                invalidos.append(counter)
            printColor.imprimir("  {:>2}. {:>15} {} X {} {:<15}   {:0>2}/{:0>2}   {:>14}".format(counter, jogo[0],
                        jogo[2] if jogo[2] >=0 else " ", jogo[3] if jogo[3] >=0 else " ", jogo[1],
                        jogo[12][0], jogo[12][1], jogo[13]), cor)
            counter+=1

    printColor.imprimir("\n\n  Digite um valor positivo para editar, 0 para voltar e negativo pa-\n  ra resetar e depois pressione <ENTER>:", "branco", False, False)
    return [fixInput(input()), counter-1, invalidos]

def menuTabela(tabela):
    """
    Desc.: Mostra a tabela da copa do mundo e seus números.
    (Pontos, aproveitamento, saldo de gols, etc.)
    """
    system("cls")
    cabecalho()
    letras = ["A","B","C","D","E","F","G","H"]
    acm = 0
    for i in letras:

        printColor.imprimir("\n  ---------------------------- GRUPO {} -----------------------------".format(i),"branco" )
        printColor.imprimir("  Classificação--------------------P--J--V--E--D--GP--GC---SG------%","branco")

        printColor.imprimir("  1ª{:.>29s}: {}  {}  {}  {}  {}  {:>2d}  {:>2d}  {:"">3d}  {:>5.1f}".format(
            tabela[acm][0][0], tabela[acm][0][1], tabela[acm][0][2], tabela[acm][0][3], tabela[acm][0][4],
            tabela[acm][0][5], tabela[acm][0][6], tabela[acm][0][7], tabela[acm][0][6]-tabela[acm][0][7],
            (100*(((tabela[acm][0][3]*3)+(tabela[acm][0][4]))/(tabela[acm][0][2]*3)) if tabela[acm][0][1]!=0 else 0)), "amarelo")

        printColor.imprimir("  2ª{:.>29s}: {}  {}  {}  {}  {}  {:>2d}  {:>2d}  {:"">3d}  {:>5.1f}".format(
            tabela[acm][1][0], tabela[acm][1][1], tabela[acm][1][2], tabela[acm][1][3], tabela[acm][1][4],
            tabela[acm][1][5], tabela[acm][1][6], tabela[acm][1][7], tabela[acm][1][6]-tabela[acm][1][7],
            (100*(((tabela[acm][1][3]*3)+(tabela[acm][1][4]))/(tabela[acm][1][2]*3)) if tabela[acm][1][1]!=0 else 0)), "amarelo")

        printColor.imprimir("  3ª{:.>29s}: {}  {}  {}  {}  {}  {:>2d}  {:>2d}  {:"">3d}  {:>5.1f}".format(
            tabela[acm][2][0], tabela[acm][2][1], tabela[acm][2][2], tabela[acm][2][3], tabela[acm][2][4],
            tabela[acm][2][5], tabela[acm][2][6], tabela[acm][2][7], tabela[acm][2][6]-tabela[acm][2][7],
            (100*(((tabela[acm][2][3]*3)+(tabela[acm][2][4]))/(tabela[acm][2][2]*3)) if tabela[acm][2][1]!=0 else 0)), "branco")

        printColor.imprimir("  4ª{:.>29s}: {}  {}  {}  {}  {}  {:>2d}  {:>2d}  {:"">3d}  {:>5.1f}".format(
            tabela[acm][3][0], tabela[acm][3][1], tabela[acm][3][2], tabela[acm][3][3], tabela[acm][3][4],
            tabela[acm][3][5], tabela[acm][3][6], tabela[acm][3][7], tabela[acm][3][6]-tabela[acm][3][7],
            (100*(((tabela[acm][3][3]*3)+(tabela[acm][3][4]))/(tabela[acm][3][2]*3)) if tabela[acm][3][1]!=0 else 0)), "branco")

        acm += 1
    printColor.imprimir("\n  Pressione <ENTER> para voltar.", 'branco', False, False)    
    fixInput(input())

def selecoesInfo(selecoes):
    """
    Desc.: Menu inicial das informações sobre as seleções.
    """
    system("cls")
    printColor.imprimir("\n  Aqui vai ser o menu incial das informações sobre\n  " +
                        "as seleções", "azul", False, False)
    fixInput(input())

def sobre():
    """
    Desc.: Mostra as informações sobre os desenvolvedores.
    """
    system("cls")
    cabecalho()
    printColor.imprimir("\n        © Copa do Mundo 2014 Versão 1.0.1 All rights reserved.", "amarelo")
    printColor.imprimir("\n                          Desenvolvido por:", "branco")
    printColor.imprimir("                            Alan J. Paiva", "amarelo")
    printColor.imprimir("                             Vitor Muniz", "amarelo")
    printColor.imprimir("\n               Em caso de bugs ou sugestões, reportar para:           ", "branco")
    printColor.imprimir("                       alan.jeferson11@gmail.com", "amarelo")
    printColor.imprimir("                         vitorfmuniz@gmail.com", "amarelo")
    printColor.imprimir("\n\n                     Pressione <ENTER> para voltar.", "branco", False, False)
    fixInput(input())

def erroOpcaoInvalida(mensagem=None, beep=True):
    """
    Desc. Exibe uma mensagem de erro opção inválida para o usuário.
    mensagem: A mensagem que será exibida como erro.
    beep: informa se deve haver um beep sinalizando o erro.
    """
    if mensagem==None:
        mensagem = "Opção inválida"
    printColor.imprimir("{}\n  {}".format("\a" if beep else "", mensagem), "vermelho", False, False)
    fixInput(input())

def editaJogo(jogo, fase=None):
    """
    Desc.: Mostra um menu para edição do jogo passado como parâmetro.
    """
    errorMessage = "Valor inválido"
    while True:
        system("cls")
        cabecalho()
        printColor.imprimir("\n  Jogo: {}   X   {}".format(jogo[0], jogo[1]), "verde")
        for i in range(2,4):
            while True:
                try:
                    printColor.imprimir("\n  Resultado de {} (<0 para cancelar):".format(jogo[i-2]), "branco", False, False)
                    jogo[i] = int(fixInput(input()))
                    if jogo[i]<0:
                        jogo[2], jogo[3] = -1, -1
                        printColor.imprimir("\n  Edição cancelada.", "vermelho", False, False)
                        printColor.imprimir("\n\n  Pressione <ENTER> para continuar.", "branco", False, False)
                        fixInput(input())
                        return None
                    break
                except ValueError:
                    erroOpcaoInvalida(errorMessage)
        if jogo[2]==jogo[3] and fase>2:
            erroOpcaoInvalida("Eliminatórias não terminam empatadas. Digite o resultado final.")
        else:
            break
    printColor.imprimir("\n  Dados editados com sucesso.", "amarelo", False, False)
    printColor.imprimir("\n\n  Pressione <ENTER> para continuar.", "branco", False, False)
    fixInput(input())
    return jogo

def listarPodium(podium):
    """
    Desc.: Recebe uma lista com os primeiros colocados e a exibe na tela
    """
    system("cls")
    cabecalho()
    printColor.imprimir("\n  Campeão: {}".format(podium[0]), "amarelo")
    printColor.imprimir("\n  2º Lugar: {}".format(podium[1]), "branco")
    printColor.imprimir("\n  3º Lugar: {}".format(podium[2]), "branco")
    printColor.imprimir("\n\n  Pressione <ENTER> para voltar.", "branco", False, False)
    fixInput(input())

def menuInfoSelecoes(selecoes):
    """
    Desc.: Mostra o menu de escolha das informações sobre as seleções.
    """
    system("cls")
    cabecalho()
    print()
    i=0
    while i<32:
        printColor.imprimir("  {:>2}. {:<30}{:>2}. {}".format(i+1, selecoes[i][0],i+2, selecoes[i+1][0]), "branco")
        i+=2
    printColor.imprimir("\n  Escolha uma opção e pressione <ENTER>: ", "branco", False, False)
    return int(fixInput(input()))

def mostraSelecaoInfo(selecao):
    """
    Desc.: Mostra as informações da seleção passada como parâmetro
    """
    system("cls")
    cabecalho()
    printColor.imprimir("\n  Nome: ", "branco", False, False)
    printColor.imprimir("{}".format(selecao[0]), "amarelo")
    printColor.imprimir("\n  Treinador: ", "branco", False, False)
    printColor.imprimir("{}".format(selecao[1]), "amarelo")
    printColor.imprimir("\n  Artilheiro(s): ", "branco", False, False)
    printColor.imprimir("{}".format(selecao[2]), "amarelo")
    printColor.imprimir("\n  Participações em copas do mundo: ", "branco", False, False)
    printColor.imprimir("{}".format(selecao[3]), "amarelo")
    printColor.imprimir("\n  Nº de títulos: ", "branco", False, False)
    printColor.imprimir("{}".format(selecao[4]), "amarelo")
    printColor.imprimir("\n  Classificação no ranking da FIFA: ", "branco", False, False)
    printColor.imprimir("{}º".format(selecao[5]), "amarelo")
    printColor.imprimir("\n  Sobre: ", "branco", False, False)
    sobre = selecao[6]
    i=0
    while len(sobre)>0:
        if i==0:
            printColor.imprimir("{} ".format(sobre[:58]), "amarelo")
            sobre = sobre[58:]
        else:
            printColor.imprimir("  {} ".format(sobre[:65]), "amarelo")
            sobre = sobre[66:]
        i+=1

    printColor.imprimir("\n  Pressione <ENTER> para continuar.", "branco", False, False)
    fixInput(input())
