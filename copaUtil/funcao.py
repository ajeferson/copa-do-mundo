import pickle
from random import randint

def qual_rod(todosJogos):
    """
    Desc.: Retorna uma lista contendo uma string e um inteiro que
    representam a rodada corrente da copa
    """
    eliminatorias = todosJogos[1]
    fasegrupos = todosJogos[0]
    if eliminatorias[-2][0][2] != -1:
        return (["Copa encerrada", 8])
    elif eliminatorias[-3][0][2]!= -1:
        return (["Final", 7])
    elif eliminatorias[-4][0][2]!= -1 and eliminatorias[-4][1][2] != -1:
        return (["Disputa de terceiro lugar", 6])
    elif eliminatorias[-5][0][2] != -1 and eliminatorias[-5][1][2] != -1 and \
         eliminatorias[-5][2][2] != -1 and eliminatorias[-5][3][2] != -1:
        return(["Semifinal", 5])
    elif eliminatorias[-6][0][2] != -1 and eliminatorias[-6][1][2] != -1 and \
         eliminatorias[-6][2][2] != -1 and eliminatorias[-6][3][2] != -1 and \
         eliminatorias[-6][4][2] != -1 and eliminatorias[-6][5][2] != -1 and \
         eliminatorias[-6][6][2] != -1 and eliminatorias[-6][7][2] != -1:
        return(["Quartas de final", 4])
    elif fasegrupos[-1][2][0][2] != -1 and fasegrupos[-1][2][1][2] != -1 and \
         fasegrupos[-2][2][0][2] != -1 and fasegrupos[-2][2][1][2] != -1 and \
         fasegrupos[-3][2][0][2] != -1 and fasegrupos[-3][2][1][2] != -1 and \
         fasegrupos[-4][2][0][2] != -1 and fasegrupos[-4][2][1][2] != -1 and \
         fasegrupos[-5][2][0][2] != -1 and fasegrupos[-5][2][1][2] != -1 and \
         fasegrupos[-6][2][0][2] != -1 and fasegrupos[-6][2][1][2] != -1 and \
         fasegrupos[-7][2][0][2] != -1 and fasegrupos[-7][2][1][2] != -1 and \
         fasegrupos[-8][2][0][2] != -1 and fasegrupos[-8][2][1][2] != -1:
        return(["Oitavas de final", 3])
    elif fasegrupos[-1][1][0][2] != -1 and fasegrupos[-1][1][1][2] != -1 and \
         fasegrupos[-2][1][0][2] != -1 and fasegrupos[-2][1][1][2] != -1 and \
         fasegrupos[-3][1][0][2] != -1 and fasegrupos[-3][1][1][2] != -1 and \
         fasegrupos[-4][1][0][2] != -1 and fasegrupos[-4][1][1][2] != -1 and \
         fasegrupos[-5][1][0][2] != -1 and fasegrupos[-5][1][1][2] != -1 and \
         fasegrupos[-6][1][0][2] != -1 and fasegrupos[-6][1][1][2] != -1 and \
         fasegrupos[-7][1][0][2] != -1 and fasegrupos[-7][1][1][2] != -1 and \
         fasegrupos[-8][1][0][2] != -1 and fasegrupos[-8][1][1][2] != -1:
        return(["Fase de Grupos/3ª rodada", 2])
    elif fasegrupos[-1][0][0][2] != -1 and fasegrupos[-1][0][1][2] != -1 and \
         fasegrupos[-2][0][0][2] != -1 and fasegrupos[-2][0][1][2] != -1 and \
         fasegrupos[-3][0][0][2] != -1 and fasegrupos[-3][0][1][2] != -1 and \
         fasegrupos[-4][0][0][2] != -1 and fasegrupos[-4][0][1][2] != -1 and \
         fasegrupos[-5][0][0][2] != -1 and fasegrupos[-5][0][1][2] != -1 and \
         fasegrupos[-6][0][0][2] != -1 and fasegrupos[-6][0][1][2] != -1 and \
         fasegrupos[-7][0][0][2] != -1 and fasegrupos[-7][0][1][2] != -1 and \
         fasegrupos[-8][0][0][2] != -1 and fasegrupos[-8][0][1][2] != -1:
        return(["Fase de Grupos/2ª rodada", 1])
    else:
        return(["Fase de Grupos/1ª rodada", 0])



def ordenaTabela(tabela, groups, random=True):
    """
    Desc.: Ordena os jogos pelos critérios de desempate  ou ,caso os arquivos estejam
    "zerados" , ordena por ordem alfabética 
    """
    for k in range(8):
        grupo = tabela[k]
        ordenada = []
        for j in range(4):
            comp = 0
            for i in range(len(grupo)):
                if(i!=comp):
                    #Verificando pela pontuação
                    if grupo[i][1]<grupo[comp][1]:
                        comp=i
                    elif grupo[i][1]==grupo[comp][1]:
                        #Verificando pelo saldo de gols
                        if (grupo[i][6]-grupo[i][7])<(grupo[comp][6]-grupo[comp][7]):
                            comp=i
                        elif (grupo[i][6]-grupo[i][7])==(grupo[comp][6]-grupo[comp][7]):
                            #Verificando pelo número gols
                            if grupo[i][6]<grupo[comp][6]:
                                comp=i
                            elif grupo[i][6]==grupo[comp][6]:
                                jogo = getConfronto(grupo[i][0], grupo[comp][0], groups[k])
                                #Verificando pelo confronto direto, resultado.
                                if jogo[2]<jogo[3]:
                                    comp=i
                                elif jogo[2]==jogo[3]:
                                    if random:
                                        #Verificação aleatória
                                        if randint(0, 1)==0:
                                            comp=i
                                    else:
                                        #Verificando pela ordem alfabética
                                        if grupo[i][0]>grupo[comp][0]:
                                            comp=i
                                
            ordenada.append(grupo[comp])
            grupo.pop(comp)
        ordenada.reverse()
        tabela[k] = ordenada
    return tabela
    

def mostrarjDatas(todosJogos,data):
    datajogos1 =[]
    """
    Desc.: Busca os jogos pela data escolhida pelo usuário
    """
    for a in todosJogos:
        if a == todosJogos[0]:
            for k in range(8):
                for j in range(3):
                    for l in range(2):
                            if todosJogos[0][k][j][l][12] == data:
                                datajogos1.append(todosJogos[0][k][j][l])  
        else:
            for i in todosJogos[1]:
                if i == todosJogos[1][0]:
                    for i in range(8):
                        if todosJogos[1][0][i][12] == data:
                            datajogos1.append(todosJogos[1][0][i])
                elif i == todosJogos[1][1]:
                    for i in range(4):
                        if todosJogos[1][1][i][12] == data:
                            datajogos1.append(todosJogos[1][1][i])
                elif i == todosJogos[1][2]:
                    for i in range (2):
                        if todosJogos[1][2][i][12] == data:
                             datajogos1.append(todosJogos[1][2][i])
                elif i == todosJogos[1][3]:
                    if data == todosJogos[1][3][0][12]:
                        datajogos1.append(todosJogos[1][3][0])
                elif i == todosJogos[1][4]:
                    if data == todosJogos[1][4][0][12]:
                        datajogos1.append(todosJogos[1][4][0])                    
    return datajogos1


                
def mostrarjTimes(todosJogos,sel):
    seljogos1 =[]
    """
    Desc.: Busca os jogos pelo nome da seleção escolhida pelo usuário
    """
    for i in (todosJogos) :
        if i == todosJogos[0]:
            for k in range(8): #Grupos
                for j in range(3): #Rodada
                    for l in range(2): #Jogo
                        if (todosJogos[0][k][j][l][0].lower() == sel.lower() or todosJogos[0][k][j][l][1].lower() == sel.lower()):
                            seljogos1.append(todosJogos[0][k][j][l])
        elif i == todosJogos[1]:
            for i in range(8): #Oitavas
                a = todosJogos[1][0][i][0].lower()
                b = todosJogos[1][0][i][1].lower()
                if a == sel.lower() or  b == sel.lower():
                    seljogos1.append(todosJogos[1][0][i])
            for i in range(4):
                x = todosJogos[1][1][i][0].lower()
                y = todosJogos[1][1][i][1].lower()
                if  x == sel.lower() or  y == sel.lower():
                    seljogos1.append(todosJogos[1][1][i])
            for i in range (2):
                v = todosJogos[1][2][i][0].lower()
                u = todosJogos[1][2][i][1].lower()
                if v == sel.lower() or u == sel.lower():
                     seljogos1.append(todosJogos[1][2][i])
            k = todosJogos[1][3][0][0].lower()
            l = todosJogos[1][3][0][1].lower()
            m = todosJogos[1][4][0][0].lower()
            n = todosJogos[1][4][0][1].lower()
            if sel.lower() == k or sel.lower() == l:
                seljogos1.append(todosJogos[1][3][0])
            if sel.lower() == m or sel.lower() == n:
                seljogos1.append(todosJogos[1][4][0])
            
    return(seljogos1)


def mostrarjGrupos(todosJogos,grupo):
    """
    Desc.: Busca os jogos pelo grupo escolhido pelo usuário
    """
    a = -1
    x = grupo
    y = ["a","b","c","d","e","f","g","h"]
    for i in y:
        a += 1
        if x[0].lower() == i:
            return todosJogos[0][a]

                      
def mostrarjFases (todosJogos,fase):
    fasejogos1 = []
    """
    Desc.: Busca os jogos pela fase escolhida pelo usuário
    """
    if fase == []:
        pass
    elif fase == "1":
        fasejogos1.append(todosJogos[1][0])
    elif fase == "2":
        fasejogos1.append(todosJogos[1][1])
    elif fase == "3":
        fasejogos1.append(todosJogos[1][2])
    elif fase == "4":
        fasejogos1.append(todosJogos[1][3])
    elif fase == "5":
        fasejogos1.append(todosJogos[1][4])
    return fasejogos1[0]
    

def getIndexJogo(todosJogos, num, rodada):
    """
    Des.: Dado um número do menu de edição de jogos e a rodada, retorna uma lista
    contendo os índices necessários pra acessar um jogo.
    """
    counter=1
    if rodada<=2:
        grupos = todosJogos[0]
        for k in range(8):
            for j in range(2):
                if counter==num:
                    return [k, rodada, j]
                counter+=1
    else:
        rodada = rodada-3
        return [rodada, num-1]


def atualizaTabela(jogo, grupo):
    """
    Desc.: Recebe um jogo, o grupo da tabela como parâmetros e faz as devidas
    atribuições de pontuação, gols pró, gols contra, etc. Retorna o grupo  atualizado.
    """
    resA = jogo[2]
    resB = jogo[3]

    if resA>resB:
        pontA = [3, 1, 0, 0]
        pontB = [0, 0, 0, 1]
    elif resB>resA:
        pontB = [3, 1, 0, 0]
        pontA = [0, 0, 0, 1]
    else:
        pontA = [1, 0, 1, 0]
        pontB = [1, 0, 1, 0]
    for lista in grupo:
        if lista[0]==jogo[0]:
            lista[1]+=pontA[0]
            lista[2]+=1
            lista[3]+=pontA[1]
            lista[4]+=pontA[2]
            lista[5]+=pontA[3]
            lista[6]+=jogo[2]
            lista[7]+=jogo[3]
        elif lista[0]==jogo[1]:
            lista[1]+=pontB[0]
            lista[2]+=1
            lista[3]+=pontB[1]
            lista[4]+=pontB[2]
            lista[5]+=pontB[3]
            lista[6]+=jogo[3]
            lista[7]+=jogo[2]
    #input(grupo)
    return grupo
    
   
def atualizaOitavas(todosJogos, tabela, grupos):
    """
    Des.: Recebe a lista de todos os jogos e a tabela, verifica quais são os primeiros lugares
    dos grupos e atualiza as oitavas de final com os times correspondentes.
    """
    oitavas = todosJogos[1][0]

    #Grupo A
    if todasAsPartidasJogadas(grupos[0]):
        oitavas[0][0] = tabela[0][0][0]
        oitavas[2][1] = tabela[0][1][0]
    else:
        oitavas[0][0], oitavas[2][1] = "", ""

    #Grupo B
    if todasAsPartidasJogadas(grupos[1]):
        oitavas[0][1] = tabela[1][1][0]
        oitavas[2][0] = tabela[1][0][0]
    else:
        oitavas[0][1], oitavas[2][0] = "", ""

    #Grupo C
    if todasAsPartidasJogadas(grupos[2]):
        oitavas[1][0] = tabela[2][0][0]
        oitavas[3][1] = tabela[2][1][0]
    else:
        oitavas[1][0], oitavas[3][1] = "", ""

    #Grupo D
    if todasAsPartidasJogadas(grupos[3]):
        oitavas[1][1] = tabela[3][1][0]
        oitavas[3][0] = tabela[3][0][0]
    else:
        oitavas[1][1], oitavas[3][0] = "", ""

    #Grupo E
    if todasAsPartidasJogadas(grupos[4]):
        oitavas[4][0] = tabela[4][0][0]
        oitavas[6][1] = tabela[4][1][0]
    else:
        oitavas[4][0], oitavas[6][1] = "", ""

    #Grupo F
    if todasAsPartidasJogadas(grupos[5]):
        oitavas[4][1] = tabela[5][1][0]
        oitavas[6][0] = tabela[5][0][0]
    else:
        oitavas[4][1], oitavas[6][0] = "", ""

    #Grupo G
    if todasAsPartidasJogadas(grupos[6]):
        oitavas[5][0] = tabela[6][0][0]
        oitavas[7][1] = tabela[6][1][0]
    else:
        oitavas[5][0], oitavas[7][1] = "", ""

    #Grupo H
    if todasAsPartidasJogadas(grupos[7]):
        oitavas[5][1] = tabela[7][1][0]
        oitavas[7][0] = tabela[7][0][0]
    else:
        oitavas[5][1], oitavas[7][0] = "", ""

    todosJogos[1][0] = oitavas

    return todosJogos

    
def atualizaQuartas(todosJogos):
    """
    Desc.: Dadas as eliminatórias, atualiza os jogos das quartas de final de acordo
    com os vencedores das oitavas.
    """
    oitavas = todosJogos[1][0]
    quartas = todosJogos[1][1]

    #Jogo 1
    if oitavas[0][2]!=-1:
        quartas[0][0] = oitavas[0][0] if oitavas[0][2] > oitavas[0][3] else oitavas[0][1]
    else:
        quartas[0][0] = ""
    if oitavas[1][2]!=-1:
        quartas[0][1] = oitavas[1][0] if oitavas[1][2] > oitavas[1][3] else oitavas[1][1]
    else:
        quartas[0][1] = ""

    #Jogo 2
    if oitavas[4][2]!=-1:
        quartas[1][0] = oitavas[4][0] if oitavas[4][2] > oitavas[4][3] else oitavas[4][1]
    else:
        quartas[1][0] = ""
    if oitavas[5][2]!=-1:
        quartas[1][1] = oitavas[5][0] if oitavas[5][2] > oitavas[5][3] else oitavas[5][1]
    else:
        quartas[1][1] = ""

    #Jogo 3
    if oitavas[2][2]!=-1:
        quartas[2][0] = oitavas[2][0] if oitavas[2][2] > oitavas[2][3] else oitavas[2][1]
    else:
        quartas[2][0] = ""
    if oitavas[3][2]!=-1:
        quartas[2][1] = oitavas[3][0] if oitavas[3][2] > oitavas[3][3] else oitavas[3][1]
    else:
        quartas[2][1] = ""

    #Jogo 4
    if oitavas[6][2]!=-1:
        quartas[3][0] = oitavas[6][0] if oitavas[6][2] > oitavas[6][3] else oitavas[6][1]
    else:
        quartas[3][0] = ""
    if oitavas[7][2]!=-1:
        quartas[3][1] = oitavas[7][0] if oitavas[7][2] > oitavas[7][3] else oitavas[7][1]
    else:
        quartas[3][1] = ""

    todosJogos[1][0] = oitavas
    todosJogos[1][1] = quartas

    return todosJogos


def atualizaSemiFinal(todosJogos):
    """
    Desc.: Dados os jogos, atualiza os nomes das seleções na semi-final conforme os resultados
    das mesmas nas quartas de final.
    """
    quartas = todosJogos[1][1]
    semi = todosJogos[1][2]

    #Jogo 1
    if quartas[0][2]!=-1:
        semi[0][0] = quartas[0][0] if quartas[0][2] > quartas[0][3] else quartas[0][1]
    else:
        semi[0][0]=""
    if quartas[1][2]!=-1:
        semi[0][1] = quartas[1][0] if quartas[1][2] > quartas[1][3] else quartas[1][1]
    else:
        semi[0][1]=""

    #Jogo 2
    if quartas[2][2]!=-1:
        semi[1][0] = quartas[2][0] if quartas[2][2] > quartas[2][3] else quartas[2][1]
    else:
        semi[1][0]=""
    if quartas[3][2]!=-1:
        semi[1][1] = quartas[3][0] if quartas[3][2] > quartas[3][3] else quartas[3][1]
    else:
        semi[1][1]=""

    todosJogos[1][1] = quartas
    todosJogos[1][2] = semi

    return todosJogos


def atualizaTerceiroLugar(todosJogos):
    """
    Desc.: Dados os jogos, atualiza os nomes das seleções que irão disputar o terceiro
    lugar conforme os resultados obtidos na semi-final. Retorna todos os jogos atualizados.
    """
    semi = todosJogos[1][2]
    terc = todosJogos[1][3]

    if semi[0][2]!=-1:
        terc[0][0] = semi[0][1] if semi[0][2] > semi[0][3] else semi[0][0]
    else:
        terc[0][0] = ""
    if semi[1][2]!=-1:
        terc[0][1] = semi[1][1] if semi[1][2] > semi[1][3] else semi[1][0]
    else:
        terc[0][1] = ""

    todosJogos[1][3] = terc

    return todosJogos


def atualizaFinal(todosJogos):
    """
    Desc.: Dados os jogos, atualiza os nomes das seleções que irão disputar a final
    conforme os resultados obtidos na semi-final. Retorna todos os jogos atualizados.
    """
    semi = todosJogos[1][2]
    final = todosJogos[1][4]
    
    if semi[0][2]!=-1:
        final[0][0] = semi[0][0] if semi[0][2] > semi[0][3] else semi[0][1]
    else:
        final[0][0] = ""
    if semi[1][2]!=-1:
        final[0][1] = semi[1][0] if semi[1][2] > semi[1][3] else semi[1][1]
    else:
        final[0][1] = ""
    todosJogos[1][4] = final

    return todosJogos


def atualizaPodium(todosJogos):
    """
    Desc.: Dados todos os jogos, atualiza os nomes do campeão, segundo e terceiro colocados.
    Retorn a lista de jogos atualizada.
    """
    terc = todosJogos[1][3]
    final = todosJogos[1][4]
    podium = todosJogos[1][5]
    if terc[0][2]!=-1:
        podium[2] = terc[0][0] if terc[0][2] > terc[0][3] else terc[0][1]
    if final[0][2]!=-1:
        podium[0], podium[1] = (final[0][0], final[0][1]) if final[0][2] > final[0][3] else (final[0][1], final[0][0])

    todosJogos[1][5] = podium

    return todosJogos


def todasAsPartidasJogadas(grupo):
    """
    Desc.: Dado um grupo, retorna se todas as partidas do mesmo já foram jogadas,
    para, em caso afirmativo, setar os valores para as oitavas de finais apropriadamente
    """
    for rodada in grupo:
        for jogo in rodada:
            if jogo[2]==-1:
                return False
    return True


def apagaJogo(jogo, grupo):
    """
    Desc. Recebe o índice de um jogo e o grupo da tabela e apaga os
    resultados. Retorna o grupo da  tabela e o jogo atualizados.
    """
    resA = jogo[2]
    resB = jogo[3]

    if resA>resB:
        decA = [3, 1, 0, 0]
        decB = [0, 0, 0, 1]
    elif resB>resA:
        decB = [3, 1, 0, 0]
        decA = [0, 0, 0, 1]
    else:
        decA = [1, 0, 1, 0]
        decB = [1, 0, 1, 0]

    for i in range(4):
        if grupo[i][0]==jogo[0]:
            selA=i
        elif grupo[i][0]==jogo[1]:
            selB=i

    grupo[selA][1]-=decA[0]
    grupo[selA][2]-=1
    grupo[selA][3]-=decA[1]
    grupo[selA][4]-=decA[2]
    grupo[selA][5]-=decA[3]
    grupo[selA][6]-=jogo[2]
    grupo[selA][7]-=jogo[3]

    grupo[selB][1]-=decB[0]
    grupo[selB][2]-=1
    grupo[selB][3]-=decB[1]
    grupo[selB][4]-=decB[2]
    grupo[selB][5]-=decB[3]
    grupo[selB][6]-=jogo[3]
    grupo[selB][7]-=jogo[2]

    jogo[2] = -1
    jogo[3] = -1

    return [jogo, grupo]


def getConfronto(selA, selB, jogos):
    """
    Desc.: Retorna o último jogo entre as seleções selA e selB.
    """
    for rodada in jogos:
        for jogo in rodada:
            if (jogo[0]==selA and jogo[1]==selB) or (jogo[1]==selA and jogo[0]==selB):
                return jogo
    return None

def fixInput(entrada):
    """
    Desc.: Corrige um bug do python no windows, que não compara
    as string direito.
    """
    return entrada.strip()
