#encoding:utf-8

import pickle
from os import system
from copaUtil.colorirTerminal import *
from copaUtil.copaArquivos import *
from copaUtil.menu import *
from copaUtil.funcao import *

#Global - Utilizada para fazer todos os prints coloridos
printColor = ColorirTerminal()

def principal():
    """
    Desc.: Função principal do programa, chama as outras funções, exibe
    menu, etc.
    """

    #Carregando arquivos
    grupos = loadGrupos()
    eliminatorias = loadEliminatorias()
    todosJogos = [grupos, eliminatorias]
    rodada_corrente = qual_rod(todosJogos)
    tabela = loadSelecoesTabela()
    info_selecoes = loadSelecoesInfo()
    #ordenaTabela(tabela, grupos)
    #atualizaOitavas(todosJogos, tabela, grupos)

    while True:
        opt = menuInicial(todosJogos,rodada_corrente)
        if opt=="1":
            while True:
                listarJogos = menuListarJogos(rodada_corrente[0])
                if listarJogos == "0":
                    break
                if listarJogos == "1":
                    while True:
                        try :
                            system("cls")
                            cabecalho()
                            printColor.imprimir("\n  Rodada atual: ", "branco", False, False)
                            printColor.imprimir("{}".format(rodada_corrente[0]), "amarelo")
                            printColor.imprimir("\n  Digite a data do jogo(Digite 0 para voltar): ","branco", False,False)
                            data = fixInput(input())
                            if data == "0":
                                break
                            data = data.split("/")
                            if data =="":
                                printColor.imprimir("\n  Data inválida","amarelo")
                                printColor.imprimir("\n  Pressione <ENTER> para continuar.", "branco", False, False)
                                fixInput(input())
                            data[0] = int(data[0])
                            data[1] = int(data[1])
                            data[2] = int(data[2])
                            if data[0] <= 0 or data[1] <=0 :
                                printColor.imprimir("\n  Data inválida","amarelo")
                                printColor.imprimir("\n  Pressione <ENTER> para continuar.", "branco", False, False)
                                fixInput(input())
                            else:
                                if data[2]==14 or data[2]==2014:
                                    jogos = mostrarjDatas(todosJogos,data[0:2])
                                else:
                                    jogos = []
                                if len(jogos)>0:
                                    printColor.imprimir("\n  Jogos Encontrados: \n", "amarelo")
                                    printajogos(jogos)
                                else:
                                    printColor.imprimir("\n  Nenhum jogo encontrado.", "amarelo")
                                printColor.imprimir("\n  Pressione <ENTER> para continuar.", "branco", False, False)
                                fixInput(input()) 
                        except ValueError:
                            erroOpcaoInvalida("Data inválida")
                        except  IndexError:
                            erroOpcaoInvalida("Data inválida")
                elif listarJogos == "2":
                    while True:
                        system("cls")
                        cabecalho()
                        printColor.imprimir("\n  Rodada atual: ", "branco", False, False)
                        printColor.imprimir("{}".format(rodada_corrente[0]), "amarelo")
                        printColor.imprimir("\n  Digite o nome da seleção(Digite 0 para voltar): ","branco", False,False)
                        data = fixInput(input())
                        if data == "0":
                            break
                        jogos = mostrarjTimes(todosJogos,data)                            
                        if len(jogos)== 0:
                            erroOpcaoInvalida("Seleção inválida","amarelo")
                        elif data == "":
                            printColor.imprimir("\n  Seleção inválida","amarelo")
                        else:
                            printColor.imprimir("\n  Jogos Encontrados: \n", "amarelo")
                            printajogos(jogos)
                            printColor.imprimir("\n  Pressione <ENTER> para continuar.", "branco", False, False)
                            fixInput(input())
            
                elif listarJogos == "3":
                    
                    while True:
                        try :
                            system("cls")
                            cabecalho()
                            printColor.imprimir("\n  Rodada atual: ", "branco", False, False)
                            printColor.imprimir("{}".format(rodada_corrente[0]), "amarelo")
                            x = ["a","b","c","d","e","f","g","h"]
                            printColor.imprimir("\n  A B C D E F G H ","branco")
                            printColor.imprimir("\n  Digite o  grupo( Digite 0 para voltar): ","branco", False,False)
                            data = fixInput(input())
                            data = data.split()
                            if data == ["0"]:
                                break
                            if data[0].lower() in x:
                                jogos = mostrarjGrupos(todosJogos,data)
                                printColor.imprimir("\n  Jogos Encontrados: \n", "amarelo")
                                printajogos(jogos)
                                printColor.imprimir("\n\n  Pressione <ENTER> para continuar.", "branco", False, False)
                                fixInput(input())
                            else:
                                erroOpcaoInvalida("Grupo inválido","amarelo")
                        except IndexError:
                            erroOpcaoInvalida("Grupo inválido","amarelo")
                        except TypeError:
                            erroOpcaoInvalida("Grupo inválido","amarelo")
                        except AttributeError:
                            erroOpcaoInvalida(" Grupo inválido","amarelo")
                            
                elif listarJogos == "4":
                    while True:
                        try :
                            system("cls")
                            cabecalho()
                            printColor.imprimir("\n  Rodada atual: ", "branco", False, False)
                            printColor.imprimir("{}".format(rodada_corrente[0]), "amarelo")
                            printColor.imprimir("\n  1. Oitavas de Final","branco",False,False)
                            printColor.imprimir("\n  2. Quartas de Final","branco",False,False)
                            printColor.imprimir("\n  3. SemiFinal","branco",False,False)
                            printColor.imprimir("\n  4. Terceiro lugar","branco",False,False)
                            printColor.imprimir("\n  5. Final ","branco",False,False)
                            printColor.imprimir("\n  0. Sair ","branco", False,False)
                            printColor.imprimir("\n\n  Escolha a fase :","branco", False,False)
                            data = fixInput(input())
                            if data == "0":
                                break
                            jogos = mostrarjFases(todosJogos,data)
                            if len(jogos)>0:
                                printColor.imprimir("\n  Jogos Encontrados: \n", "amarelo")
                                printajogos(jogos)
                                printColor.imprimir("\n\n  Pressione <ENTER> para continuar.", "branco")
                                fixInput(input())
                        except IndexError:
                            erroOpcaoInvalida("Escolha inválida","amarelo")
                        except AttributeError:
                            erroOpcaoInvalida("Escolha inválida","amarelo")
                            
                    
                    
                    
                
        elif opt=='2':
            if rodada_corrente[1]==8:
                erroOpcaoInvalida("Opção indisponível. A copa já foi encerrada.")
            else:
                while True:
                    try:
                        lista = menuJogosRodada(todosJogos, rodada_corrente)
                        numJogo = int(lista[0])
                        total = int(lista[1])
                        invalidos = lista[2]
                        if numJogo==0:
                            break
                        elif numJogo>total:
                            erroOpcaoInvalida()
                        elif numJogo in invalidos:
                            erroOpcaoInvalida("Opção inválida. Jogo já realizado.")
                            
                        else:
                            #Reset
                            if numJogo<0:
                                if -numJogo not in invalidos:
                                    erroOpcaoInvalida("Opção inválida.")
                                else:
                                    indices = getIndexJogo(todosJogos, -numJogo, rodada_corrente[1])
                                    if rodada_corrente[1]<=2:
                                        jogo = grupos[indices[0]][indices[1]][indices[2]]
                                        lis = apagaJogo(jogo, tabela[indices[0]])
                                        grupos[indices[0]][indices[1]][indices[2]] = lis[0]
                                        tabela[indices[0]] = lis[1]
                                        saveGrupos(grupos)
                                        saveSelecoesTabela(ordenaTabela(tabela, grupos, random=False))
                                        if rodada_corrente[1]==2:
                                            todosJogos = atualizaOitavas(todosJogos, tabela, grupos)
                                            saveEliminatorias(todosJogos[1])
                                    else:
                                        eliminatorias[indices[0]][indices[1]][2] = -1
                                        eliminatorias[indices[0]][indices[1]][3] = -1
                                        if rodada_corrente[1]==3:
                                            todosJogos = atualizaQuartas(todosJogos)
                                        elif rodada_corrente[1]==4:
                                            todosJogos = atualizaSemiFinal(todosJogos)
                                        elif rodada_corrente[1]==5:
                                            todosJogos = atualizaTerceiroLugar(todosJogos)
                                            todosJogos = atualizaFinal(todosJogos)
                                        saveEliminatorias(todosJogos[1])
                                    printColor.imprimir("\n  Dados resetados com sucesso.", "amarelo")
                                    printColor.imprimir("\n  Pressione <ENTER> para continuar.", 'branco', False, False)
                                    fixInput(input())

                                        
                            #Edição
                            elif numJogo>0:
                                indices = getIndexJogo(todosJogos, numJogo, rodada_corrente[1])
                                if rodada_corrente[1]<=2:
                                    jogoEscolhido = grupos[indices[0]][indices[1]][indices[2]]
                                    jogoEdited = editaJogo(jogoEscolhido, rodada_corrente[1])
                                    if jogoEdited!=None:
                                        grupos[indices[0]][indices[1]][indices[2]] = jogoEdited
                                        saveGrupos(grupos)
                                        if rodada_corrente[1]<=2:
                                            grupoAtualizado = atualizaTabela(jogoEdited, tabela[indices[0]])
                                            tabela[indices[0]] = grupoAtualizado
                                            tabela =  ordenaTabela(tabela, grupos)
                                            saveSelecoesTabela(tabela)
                                        todosJogos[0]=grupos
                                else:
                                    jogoEscolhido = eliminatorias[indices[0]][indices[1]]
                                    jogoEdited = editaJogo(jogoEscolhido, rodada_corrente[1])
                                    if jogoEdited!=None:
                                        eliminatorias[indices[0]][indices[1]] = jogoEdited
                                        saveEliminatorias(eliminatorias)
                                if jogoEdited!=None:
                                    #Atualizando Rodadas
                                    if rodada_corrente[1]>=2:
                                        if rodada_corrente[1]==2:
                                            todosJogos = atualizaOitavas(todosJogos, tabela, grupos)
                                        elif rodada_corrente[1]==3:
                                            todosJogos = atualizaQuartas(todosJogos)
                                        elif rodada_corrente[1]==4:
                                            todosJogos = atualizaSemiFinal(todosJogos)
                                        elif rodada_corrente[1]==5:
                                            todosJogos = atualizaTerceiroLugar(todosJogos)
                                            todosJogos = atualizaFinal(todosJogos)
                                        elif rodada_corrente[1]==6 or rodada_corrente[1]==7:
                                            todosJogos = atualizaPodium(todosJogos)
                                        saveEliminatorias(todosJogos[1])
                                        rodada_corrente = qual_rod(todosJogos)
                                        if rodada_corrente[1]==8:
                                            break
                                    rodada_corrente = qual_rod(todosJogos)
                    except ValueError:
                        if lista[0]!="":
                            erroOpcaoInvalida()
                    
        elif opt=="3":
            menuTabela(tabela)
            
        elif opt=="4":
            if rodada_corrente[1]<8:
                erroOpcaoInvalida("Opção indisponível. A copa está em andamento.")
            else:
                listarPodium(todosJogos[1][-1])

        elif opt=="5":
            while True:
                try:
                    sel = menuInfoSelecoes(info_selecoes)
                    if sel==0:
                        break
                    if sel<1 or sel>32:
                        erroOpcaoInvalida()
                    else:
                        mostraSelecaoInfo(info_selecoes[sel-1])
                except ValueError:
                    erroOpcaoInvalida()
            
        elif opt=="6":
            sobre()
            
        elif opt=="0":
            printColor.imprimir("\n  Aplicação finalizada.", "amarelo")
            printColor.imprimir("\n  Pressione ENTER para sair.", "branco", False, False)
            fixInput(input())
            break

principal()
