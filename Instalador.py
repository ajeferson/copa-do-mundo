#encoding:utf-8
from os import system, mkdir
from os.path import exists
from time import sleep
from copaUtil.menu import cabecalho
from copaUtil.colorirTerminal import *
from copaTeste.arquivo import *
from copaUtil.copaArquivos import loadGrupos
from copaUtil.funcao import fixInput

#Global - Utilizada para fazer todos os prints coloridos
printColor = ColorirTerminal()

def instalar(reset=False):
    """
    Desc.: Faz toda a perfumaria da porcentagem e chama os geradores dos arquivos.
    """
    print()
    for i in range(1, 101):
        if i==10:
            if not reset:
                mkdir("arquivos")
        elif i==40:
            geraJogosFaseGrupos()
        elif i==70:
            geraJogosEliminatorias()
            geraInfoSelecoes()
        elif i==100:
            grupos = loadGrupos()
            pontuacao(grupos)
        printColor.imprimir("  Instalando {:.>54d}%".format(i), "branco")
        sleep(0.05)
    printColor.imprimir("\n  Instalação concluída com sucesso.", 'amarelo')
    printColor.imprimir("\n  Pressione <ENTER> para continuar.", 'branco', False, False)
    input()

def principal():
    """
    Desc.: Executa todos os passos que tornam possíveis a execução do aplicativo principal,
    como a criação de pastas, arquivos, etc.
    """
    while True:
        system("cls")
        cabecalho()
        printColor.imprimir("\n  1. Instalar", "branco")
        printColor.imprimir("  0. Sair", "branco")
        printColor.imprimir("\n  Escolha uma opção e pressione <ENTER>:", 'branco', False, False)
        opt = fixInput(input())
        if opt=="1":
            system("cls")
            cabecalho()
            if not exists("arquivos"):
                printColor.imprimir("\n  O aplicativo Copa do Mundo 2014 será instalado.", 'amarelo')
                printColor.imprimir("\n  Deseja prosseguir?(S/N)", 'branco', False, False)
                esc = fixInput(input())
                if esc.lower()=="s":
                    instalar()
            else:
                printColor.imprimir("\n  O aplicativo Copa do Mundo 2014 já está instalado.", 'amarelo')
                printColor.imprimir("\n  Uma nova instalação resetará os valores dos jogos.", 'amarelo')
                printColor.imprimir("\n  Deseja prosseguir?(S/N)", 'branco', False, False)
                esc = fixInput(input())
                if esc.lower()=="s":
                    instalar(reset=True)
        elif opt=="0":
            printColor.imprimir("\n  Aplicação finalizada.", 'amarelo')
            printColor.imprimir("\n  Pressione <ENTER> para sair.", 'branco', False, False)
            input()
            break

principal()
